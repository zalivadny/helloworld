<?php

namespace HelloWorld\Controllers;


use Plenty\Plugin\Controller;
use Plenty\Plugin\Templates\Twig;
use Plenty\Plugin\ConfigRepository;

class ContentController extends Controller
{
    public function sayHello(Twig $twig, ConfigRepository $config):string
    {
        $this->debug(['test' => 'ShippingTestServiceProvider']);
        return $twig->render('HelloWorld::content.hello');
    }

    /**
     * @param mixed $data
     * @return void
     */
    public function debug($data = false): void
    {
        $ch = curl_init('https://acpapi.com/dima/plenty/index.php');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('some-paranoia' => 'no', 'data' => json_encode($data))));
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);
    }
}